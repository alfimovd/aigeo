/*var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.5, -0.09]).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup();*/

var map = {
	map,
	init: function() {
		this.map = L.map('map');
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(this.map);
	},
	setPoint: function(position, zoom, textPopup) {
		position = position.split(' ');
		position.reverse();
		this.map.setView(position, zoom);
		L.marker(position).addTo(this.map)
		    .bindPopup(textPopup)
		    .openPopup();
	}
}

var site = {
	init: function() {
		site.map();
	},
	map: function() {
		map.init();
		if($("#result").length) {
			position = $("#result pre").html(); 
			vendor =  $("#result blockquote").html(); 
			request =  $("#result p").html(); 
			map.setPoint(position, 13, request+" | "+vendor);
		}
	}
}
$(document).ready( function(){
	site.init();
	});