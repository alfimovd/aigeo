# -*- coding: utf-8 -*-

from httplib2 import iri2uri
import json
import httplib, urllib

class YandexCoordinateSearcher():
    def getCoordinate(self, address) :
        u = urllib.urlopen( self.iri_to_uri('https://geocode-maps.yandex.ru/1.x/?format=json&geocode='+ address.replace(' ','+') ) )
        data = json.load(u)
        if data['response']['GeoObjectCollection']['metaDataProperty']['GeocoderResponseMetaData']['found'] == 0:
            return False
        else :
            return data['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos']

    def getVendor(self):
        return "Яндекс"

    def iri_to_uri(self, iri):
        """Transform a unicode iri into a ascii uri."""
        if not isinstance(iri, unicode):
            raise TypeError('iri %r should be unicode.' % iri)
        return bytes(iri2uri(iri))