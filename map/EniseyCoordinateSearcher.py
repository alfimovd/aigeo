# -*- coding: utf-8 -*-

from httplib2 import iri2uri
from xml.dom import minidom
import json
import httplib, urllib

class EniseyCoordinateSearcher():
    uKey = 'jyclwyom3lwicuz0pf22sznx'
    sKey = False
    def getCoordinate(self, address) :
        if not(self.sKey) :
            self.getSKey()
        data  = urllib.urlopen( self.iri_to_uri('http://geo.24bpd.ru/services/service.php?api=1.03&service=geocode&skey=' + self.sKey + '&q='+ address) ).read()
        xmldoc = minidom.parseString(data)
        result = xmldoc.getElementsByTagName('geocenter')
        if len(result) == 0:
            return False
        else : 
            result = result[0].toxml()
            result = result.replace("<geocenter>POINT(","")
            result = result.replace(")</geocenter>","")
            return result

    def getSKey(self) :
        data  = urllib.urlopen('http://geo.24bpd.ru/services/tests/1.03/service.php?api=1.03&service=auth&call=open&ukey=' + self.uKey ).read()
        xmldoc = minidom.parseString(data)
        self.sKey = xmldoc.getElementsByTagName('session')[0].attributes['skey'].value

    def getVendor(self):
        return "Енисей ГИС"

    def iri_to_uri(self, iri):
        """Transform a unicode iri into a ascii uri."""
        if not isinstance(iri, unicode):
            raise TypeError('iri %r should be unicode.' % iri)
        return bytes(iri2uri(iri))