# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse
from YandexCoordinateSearcher import YandexCoordinateSearcher
from EniseyCoordinateSearcher import EniseyCoordinateSearcher


def index(request):
    address = request.GET.get('address', '') 
    context = {
            'request': False,
        }
    if address:
        CoordinateSearchers = []
        CoordinateSearchers.append( EniseyCoordinateSearcher() )
        CoordinateSearchers.append( YandexCoordinateSearcher() )

        for Searcher in CoordinateSearchers:
            result = Searcher.getCoordinate( address )
            if result:
                vendor =  Searcher.getVendor()
                break
        context = {
            'result': result,
            'vendor': vendor,
            'request': address,
        }
    return render(request, 'map/index.html', context)